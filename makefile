SHELL = /bin/bash
CC = gcc
CFLAGS = -g -pthread
SRC = client.c
EXE = $(patsubst %.c, %, $(SRC)) server

all: ${EXE}

server: server.c user.c game.c
	${CC} ${CFLAGS} $@.c -o $@

%:	%.c
	${CC} ${CFLAGS} $@.c -o $@

clean:
	rm ${EXE}

