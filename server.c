#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <ctype.h>

#include <limits.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <semaphore.h>
#include <sys/queue.h>

#include <arpa/inet.h>
#include <sys/socket.h>

#include "user.c"
#include "game.c"

#define SERV_PORT 10210

#define BUF_SIZE 1024

void handle_error() {
    fprintf(stderr, "Error: %s\n", strerror(errno));
    return;
}

struct conn_info_t;

void handle_connect(struct conn_info_t *conn_info);

void *create_shared_memory(size_t size) {
    // Our memory buffer will be readable and writable:
    int protection = PROT_READ | PROT_WRITE;

    // The buffer will be shared (meaning other processes can access it), but
    // anonymous (meaning third-party processes cannot obtain an address for
    // it), so only this process and its children will be able to use it:
    int visibility = MAP_SHARED | MAP_ANONYMOUS;

    // The remaining parameters to `mmap()` are not important for this use case,
    // but the manpage for `mmap` explains their purpose.
    return mmap(NULL, size, protection, visibility, -1, 0);
}

/**
 * User definitions
 */

#define MAX_USER 10

user_t users[MAX_USER];
int userCount = 0;

void gen_user() {
    users[0].user_id = 0;
    users[0].username = "test";
    users[0].passwd = "test";

    users[1].user_id = 1;
    users[1].username = "user";
    users[1].passwd = "user";

    users[2].user_id = 2;
    users[2].username = "abc";
    users[2].passwd = "abc";

    userCount = 3;
}

typedef struct conn_info_t {
    int pid;
    int connfd;
    int pipefd[2];
    user_t *user;
    game_t *current_game;

    LIST_ENTRY(conn_info_t) entries;
} conn_info_t;

#define conn_info_pools_sz 100
void *conn_info_pools;
int conn_info_pools_used[conn_info_pools_sz];

conn_info_t *create_conn_info() {
    int idx = -1;
    for (int i = 0; i < conn_info_pools_sz; i++) {
        if (!conn_info_pools_used[i]) {
            idx = i;
            conn_info_pools_used[i] = 1;
            break;
        }
    }

    if (idx == -1) {
        fprintf(stderr, "Error on creating conn_info: no space\n");
        return NULL;
    }

    conn_info_t *info = ((conn_info_t *)conn_info_pools) + idx;

    info->user = NULL;
    info->current_game = NULL;

    return info;
}

void free_conn_info(conn_info_t *info) {
    int idx = info - ((conn_info_t *)conn_info_pools);

    memset(info, 0, sizeof(conn_info_t));
    conn_info_pools_used[idx] = 0;
}

LIST_HEAD(conn_info_list_head, conn_info_t);

typedef struct {
    struct conn_info_list_head conn_infos;
    struct game_list_head games;
} shared_mem;

int conn_count = 0;

shared_mem *shmem;
sem_t shmem_mutex;

int parent_pipe_fd[2];

// game func

void write_game_status(game_t *game, char *buf) {
    sprintf(&buf[strlen(buf)], "%d %s\n%d %s\n", game->player[0]->user->user_id,
            game->player[0]->user->username, game->player[1]->user->user_id,
            game->player[1]->user->username);

    sprintf(&buf[strlen(buf)], "%d\n", game->turn);
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            sprintf(&buf[strlen(buf)], "%d ", game->board[i][j]);
        }
        sprintf(&buf[strlen(buf)], "\n");
    }
}

// signal handler def
void child_handler(int signum);
void sigusr1_handler(int signum);

void child_handler(int signum) {
    pid_t pid;
    int status;

    while ((pid = waitpid(-1, &status, WNOHANG)) > 0) {
        int es = WEXITSTATUS(status);
        printf("PID %d exit with status = %d\n", pid, es);

        sem_wait(&shmem_mutex);
        conn_info_t *conn;
        LIST_FOREACH(conn, &shmem->conn_infos, entries) {
            if (conn->pid == pid) {
                LIST_REMOVE(conn, entries);

                if (close(conn->pipefd[1]) < 0) {
                    fprintf(stderr, "Error on closing pipe of pid %d: ", pid);
                    handle_error();
                }

                free_conn_info(conn);
                break;
            }
        }
        sem_post(&shmem_mutex);
    }
}

conn_info_t *this_conn_info = NULL;

int main(int argc, char **argv) {
    in_addr_t bind_addr = htonl(INADDR_ANY);
    in_port_t bind_port = htons(SERV_PORT);

    if (argc >= 3) {
        // have a addr arg
        int idx = argc - 2;
        if ((bind_addr = inet_addr(argv[idx])) == INADDR_NONE) {
            fprintf(stderr, "Error: Invalid bind address\n");
            exit(1);
        }
    }

    if (argc >= 2) {
        // have a port arg
        int idx = argc - 1;
        int port_num = atoi(argv[idx]);

        if (port_num <= 0) {
            fprintf(stderr, "Error: Invalid port number\n");
            exit(1);
        }
        bind_port = htons(port_num);
    }

    int listenfd, connfd;
    struct sockaddr_in servaddr;

    // socket
    {
        listenfd = socket(AF_INET, SOCK_STREAM, 0);
        int reuseAddrOption = 1;
        setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &reuseAddrOption,
                   sizeof(reuseAddrOption));

        bzero(&servaddr, sizeof(servaddr));
        servaddr.sin_family = AF_INET;
        servaddr.sin_addr.s_addr = bind_addr;
        servaddr.sin_port = bind_port;
    }

    // bind
    if (bind(listenfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) == -1) {
        fprintf(stderr, "Unable to bind: ");
        handle_error();
        exit(1);
    }

    // listen
    if (listen(listenfd, 10) == -1) {
        fprintf(stderr, "Unable to listen: ");
        handle_error();
        exit(1);
    }

    // register SIGCHLD
    {
        struct sigaction sa;
        sigemptyset(&sa.sa_mask);
        sa.sa_flags = 0;
        sa.sa_handler = child_handler;
        sigaction(SIGCHLD, &sa, NULL);
    }

    // print listening info
    {
        char addr[INET_ADDRSTRLEN];
        inet_ntop(AF_INET, &(servaddr.sin_addr.s_addr), addr, INET_ADDRSTRLEN);

        fprintf(stderr, "Listening on %s:%d\n", addr, ntohs(servaddr.sin_port));
    }

    gen_user();

    // initialize shared_memory
    shmem = create_shared_memory(sizeof(shared_mem));
    LIST_INIT(&shmem->conn_infos);
    LIST_INIT(&shmem->games);

    if (sem_init(&shmem_mutex, 1, 1) < 0) {
        fprintf(stderr, "Unable to init mutex: ");
        handle_error();
        exit(1);
    }

    // register SIGUSR1
    {
        struct sigaction sa;
        sigemptyset(&sa.sa_mask);
        sa.sa_flags = 0 | SA_RESTART;
        sa.sa_handler = sigusr1_handler;
        sigaction(SIGUSR1, &sa, NULL);
    }

    // open parent pipe
    {
        if (pipe(parent_pipe_fd) < 0) {
            fprintf(stderr, "Error opening parent pipe: ");
            handle_error();
            exit(1);
        }

        int flags = fcntl(parent_pipe_fd[0], F_GETFL, 0);
        if (fcntl(parent_pipe_fd[0], F_SETFL, flags | O_NONBLOCK) < 0) {
            handle_error();
            exit(1);
        }
    }

    // init conn_info_pools
    conn_info_pools =
        create_shared_memory(sizeof(conn_info_t) * conn_info_pools_sz);

    // init game_pools
    game_pools = create_shared_memory(sizeof(game_t) * game_pools_sz);

    while (1) {
        struct sockaddr_storage client;
        size_t client_sock_len = sizeof(client);

        if ((connfd = accept(listenfd, (struct sockaddr *)&client,
                             (socklen_t *)&client_sock_len)) < 0) {
            if (errno == EINTR) continue;
            fprintf(stderr, "Unable to accept: ");
            handle_error();
            continue;
        }

        // get peer info
        {
            socklen_t len;
            struct sockaddr_storage addr;
            char ipstr[INET6_ADDRSTRLEN];
            int peerPort;

            len = sizeof addr;

            if (addr.ss_family == AF_INET) {
                // AF_INET
                struct sockaddr_in *s = (struct sockaddr_in *)&addr;
                peerPort = ntohs(s->sin_port);
                inet_ntop(AF_INET, &s->sin_addr, ipstr, sizeof ipstr);
            } else {
                // AF_INET6
                struct sockaddr_in6 *s = (struct sockaddr_in6 *)&addr;
                peerPort = ntohs(s->sin6_port);
                inet_ntop(AF_INET6, &s->sin6_addr, ipstr, sizeof ipstr);
            }

            printf("New connection: %s\nConnection count: %d\n", ipstr,
                   conn_count);
        }

        // create conn_info
        conn_info_t *conn_info = create_conn_info();
        conn_info->connfd = connfd;
        // init child pipe
        {
            if (pipe(conn_info->pipefd) < 0) {
                fprintf(stderr, "Unable to open pipe: ");
                handle_error();
                continue;
            }

            int flags = fcntl(conn_info->pipefd[0], F_GETFL, 0);
            if (fcntl(conn_info->pipefd[0], F_SETFL, flags | O_NONBLOCK) < 0) {
                handle_error();
                exit(1);
            }
        }

        sem_wait(&shmem_mutex);
        LIST_INSERT_HEAD(&shmem->conn_infos, conn_info, entries);
        sem_post(&shmem_mutex);

        pid_t childpid = fork();
        if (childpid < 0) {
            fprintf(stderr, "Unable to fork: ");
            handle_error();
            continue;
        } else if (childpid == 0) {
            /* child */

            conn_info->pid = getpid();
            this_conn_info = conn_info;

            // register SIGUSR1
            {
                struct sigaction sa;
                sigemptyset(&sa.sa_mask);
                sa.sa_flags = 0 | SA_RESTART;
                sa.sa_handler = sigusr1_handler;
                sigaction(SIGUSR1, &sa, NULL);
            }

            handle_connect(conn_info);

        Close:

            shutdown(connfd, SHUT_WR);
            shutdown(connfd, SHUT_RDWR);
            close(connfd);

            exit(0);
        }

        close(conn_info->pipefd[0]);
    }

    return 0;
}

char *str_to_vec_ptr = NULL;
int str_to_vec(char *str, char **vector) {
    if (str != NULL) {
        str_to_vec_ptr = str;
    }

    if (str_to_vec_ptr == NULL) return 0;

    char *buf = strtok(str_to_vec_ptr, "\n");
    if (buf == NULL) {
        str_to_vec_ptr = NULL;
        return 0;
    }

    str_to_vec_ptr = buf + strlen(buf) + 1;

    int cmdc = 0;
    {
        char *tok = strtok(buf, " ");
        while (tok != NULL) {
            vector[cmdc] = tok;
            cmdc++;
            tok = strtok(NULL, " ");
        }
    }
    return cmdc;
}

// pipes func
void write_to_pipe(conn_info_t *conn, char *buf);
void write_client(int connfd, char *msg);

typedef void(pipe_handler)(int msgc, char **msgv);

void pipe_parent_inv(int msgc, char **msgv);
void pipe_parent_ack_inv(int msgc, char **msgv);
void pipe_parent_rst_inv(int msgc, char **msgv);
void pipe_parent_notify_game_update(int msgc, char **msgv);

void pipe_child_inv(int msgc, char **msgv);
void pipe_child_inv_rst(int msgc, char **msgv);
void pipe_child_game_update(int msgc, char **msgv);

char *parent_pipe_cmds[] = {"INV", "INVACK", "INVRST", "GAME_UPDATE"};
pipe_handler *parent_pipe_handlers[] = {pipe_parent_inv, pipe_parent_ack_inv,
                                        pipe_parent_rst_inv,
                                        pipe_parent_notify_game_update};
int parent_cmds_count = 4;

char *child_pipe_cmds[] = {"INV", "INVRST", "GAME_UPDATE"};
pipe_handler *child_pipe_handlers[] = {pipe_child_inv, pipe_child_inv_rst,
                                       pipe_child_game_update};
int child_cmds_count = 3;

void sigusr1_handler(int signum) {
    int fd;
    if (this_conn_info != NULL) {
        // child
        fd = this_conn_info->pipefd[0];
    } else {
        // parent
        fd = parent_pipe_fd[0];
    }

    char rec_buf[BUF_SIZE] = {0};
    int bytes_read;
    int try = 0;
    while ((bytes_read = read(fd, rec_buf, BUF_SIZE - 1)) > 0) {
        printf("Pipe recv: %s\n", rec_buf);

        char *msgv[BUF_SIZE];
        int msgc = str_to_vec(rec_buf, msgv);

        while (msgc > 0) {
            for (int i = 0; i < (this_conn_info == NULL ? parent_cmds_count
                                                        : child_cmds_count);
                 i++) {
                if (strcmp(msgv[0], this_conn_info == NULL
                                        ? parent_pipe_cmds[i]
                                        : child_pipe_cmds[i]) == 0) {
                    (*(this_conn_info == NULL ? parent_pipe_handlers
                                              : child_pipe_handlers)[i])(msgc,
                                                                         msgv);
                    break;
                }
            }

            msgc = str_to_vec(NULL, msgv);
        }

        memset(rec_buf, 0, sizeof(rec_buf));
    }

    if (bytes_read < 0 && errno != EAGAIN) {
        fprintf(stderr, "Error on reading pipe: ");
        handle_error();
    }
}

void write_to_pipe(conn_info_t *conn, char *buf) {
    if (write(conn == NULL ? parent_pipe_fd[1] : conn->pipefd[1], buf,
              strlen(buf)) < 0) {
        fprintf(stderr, "Error sending to pipe of pid %d%s: ", conn->pid,
                conn == NULL ? "(parent)" : "");
        handle_error();
        return;
    }

    pid_t target_pid = conn == NULL ? getppid() : conn->pid;
    if (kill(target_pid, SIGUSR1) < 0) {
        fprintf(stderr, "Error sending signal to pid %d%s: ", target_pid,
                conn == NULL ? "(parent)" : "");
        handle_error();
        return;
    }
}

// pipe parent handlers
void pipe_parent_inv(int msgc, char **msgv) {
    int target_user_id = atoi(msgv[2]);
    conn_info_t *target_conn = NULL;

    sem_wait(&shmem_mutex);
    {
        conn_info_t *p;
        LIST_FOREACH(p, &shmem->conn_infos, entries)
        if (p != NULL && p->user != NULL &&
            p->user->user_id == target_user_id) {
            target_conn = p;
            break;
        }
    }
    sem_post(&shmem_mutex);

    if (target_conn != NULL && target_conn->current_game == NULL) {
        char buf[BUF_SIZE] = {0};
        sprintf(buf, "INV %s %s\n", msgv[1], msgv[2]);
        write_to_pipe(target_conn, buf);
    }
}
void pipe_parent_ack_inv(int msgc, char **msgv) {
    int target_user_id = atoi(msgv[2]);
    int source_conn_pid = atoi(msgv[1]);
    conn_info_t *target_conn = NULL, *source_conn = NULL;

    sem_wait(&shmem_mutex);
    {
        conn_info_t *p;
        LIST_FOREACH(p, &shmem->conn_infos, entries)
        if (p != NULL && p->user != NULL &&
            p->user->user_id == target_user_id) {
            target_conn = p;
            break;
        }

        LIST_FOREACH(p, &shmem->conn_infos, entries)
        if (p != NULL && p->pid == source_conn_pid) {
            source_conn = p;
            break;
        }
    }
    sem_post(&shmem_mutex);

    if (target_conn != NULL && source_conn != NULL) {
        // Initialize game
        game_t *game = create_game();
        game->player[0] = target_conn;
        game->player[1] = source_conn;

        sem_wait(&shmem_mutex);
        LIST_INSERT_HEAD(&shmem->games, game, entries);
        sem_post(&shmem_mutex);

        target_conn->current_game = game;
        source_conn->current_game = game;

        write_to_pipe(target_conn, "GAME_UPDATE\n");
        write_to_pipe(source_conn, "GAME_UPDATE\n");
    }
}
void pipe_parent_rst_inv(int msgc, char **msgv) {
    int target_user_id = atoi(msgv[2]);
    conn_info_t *target_conn = NULL;

    sem_wait(&shmem_mutex);
    {
        conn_info_t *p;
        LIST_FOREACH(p, &shmem->conn_infos, entries)
        if (p != NULL && p->user != NULL &&
            p->user->user_id == target_user_id) {
            target_conn = p;
            break;
        }
    }
    sem_post(&shmem_mutex);

    if (target_conn != NULL) {
        char buf[BUF_SIZE] = {0};
        sprintf(buf, "INVRST %s %s\n", msgv[1], msgv[2]);
        write_to_pipe(target_conn, buf);
    }
}
void pipe_parent_notify_game_update(int msgc, char **msgv) {
    int pid = atoi(msgv[1]);

    conn_info_t *conn = NULL;

    sem_wait(&shmem_mutex);
    {
        conn_info_t *p;
        LIST_FOREACH(p, &shmem->conn_infos, entries)
        if (p->pid == pid) {
            conn = p;
            break;
        }
    }
    sem_post(&shmem_mutex);

    if (conn != NULL && conn->current_game != NULL) {
        game_t *game = conn->current_game;

        int game_end = 0;

        for (int i = 0; i < 3; i++) {
            if (game->board[0][i] != 0 &&
                game->board[0][i] == game->board[1][i] &&
                game->board[1][i] == game->board[2][i]) {
                game_end = 1;
                break;
            }

            if (game->board[i][0] != 0 &&
                game->board[i][0] == game->board[i][1] &&
                game->board[i][1] == game->board[i][2]) {
                game_end = 1;
                break;
            }
        }

        if (game->board[0][0] != 0 && game->board[0][0] == game->board[1][1] &&
            game->board[1][1] == game->board[2][2]) {
            game_end = 1;
        }

        if (game->board[2][0] != 0 && game->board[2][0] == game->board[1][1] &&
            game->board[1][1] == game->board[0][2]) {
            game_end = 1;
        }

        for (int i = 0; i < 2; i++) {
            write_to_pipe(game->player[i],
                          game_end ? "GAME_UPDATE 1" : "GAME_UPDATE 0");
        }
    }
}

// pipe child handlers
void pipe_child_inv(int msgc, char **msgv) {
    int source_pid = atoi(msgv[1]);
    int target_user_id = atoi(msgv[2]);

    if (this_conn_info->user != NULL &&
        target_user_id == this_conn_info->user->user_id) {
        // valid invite
        conn_info_t *source_conn = NULL;

        sem_wait(&shmem_mutex);
        {
            conn_info_t *p;
            LIST_FOREACH(p, &shmem->conn_infos, entries) {
                if (p != NULL && p->pid == source_pid) {
                    source_conn = p;
                    break;
                }
            }
        }
        sem_post(&shmem_mutex);

        if (source_conn != NULL && source_conn->user != NULL) {
            char buf[BUF_SIZE] = {0};
            sprintf(buf, "RECV_INVITE %d %s\n", source_conn->user->user_id,
                    source_conn->user->username);

            write_client(this_conn_info->connfd, buf);
        }
    }
}
void pipe_child_inv_rst(int msgc, char **msgv) {
    write_client(this_conn_info->connfd, "RST_INVITE\n");
}
void pipe_child_game_update(int msgc, char **msgv) {
    int game_end = 0;
    if (msgc == 2) {
        game_end = atoi(msgv[1]);
    }

    char buf[BUF_SIZE] = {0};
    strcat(buf, "GAME_INFO\n");
    write_game_status(this_conn_info->current_game, buf + strlen(buf));
    write_client(this_conn_info->connfd, buf);

    if (game_end) {
        if (this_conn_info->current_game->player[0] == this_conn_info) {
            this_conn_info->current_game->player[0] = NULL;
        } else {
            this_conn_info->current_game->player[1] = NULL;
        }

        if (this_conn_info->current_game->player[0] == NULL &&
            this_conn_info->current_game->player[1] == NULL) {
            free_game(this_conn_info->current_game);
        }

        this_conn_info->current_game = NULL; 
    }
}

// utils
void write_client(int connfd, char *msg) {
    if (write(connfd, msg, strlen(msg)) < 0) {
        fprintf(stderr, "Error on write_client: ");
        handle_error();
        return;
    }
}

void handle_client_error(int connfd, char *error_msg) {
    write_client(connfd, error_msg);
}

// cmd_handlers
typedef int(cmd_handler)(int cmdc, char **cmdv);

int handle_login(int cmdc, char **cmdv);
int get_online(int cmdc, char **cmdv);
int invite_game(int cmdc, char **cmdv);
int ack_invite(int cmdc, char **cmdv);
int rst_invite(int cmdc, char **cmdv);
int place_chest(int cmdc, char **cmdv);
int handle_logout(int cmdc, char **cmdv);

char *commands[] = {"LOGIN",      "ONLINE",      "INVITE", "ACK_INVITE",
                    "RST_INVITE", "PLACE_CHEST", "LOGOUT"};
cmd_handler *command_handlers[] = {handle_login, get_online, invite_game,
                                   ack_invite,   rst_invite, place_chest,
                                   handle_logout};
int commands_count = 7;

int user_login_middleware() {
    if (this_conn_info->user == NULL) {
        handle_client_error(this_conn_info->connfd, "Error: login first\n");
        return 0;
    }

    return 1;
}

void handle_connect(conn_info_t *conn_info) {
    char buf[BUF_SIZE];
    memset(buf, 0, sizeof(buf));

    int bytes_read;
    while ((bytes_read = recv(conn_info->connfd, buf,
                              (sizeof(buf) / sizeof(char)) - 1, 0)) > 0) {
        char *cmdv[BUF_SIZE];
        int cmdc = str_to_vec(buf, cmdv);

        while (cmdc > 0) {
            // handle cmd
            if (cmdc > 0) {
                int flag = 0;
                for (int i = 0; i < commands_count; i++) {
                    if (strcmp(cmdv[0], commands[i]) == 0) {
                        flag = 1;
                        int ret_val = (*command_handlers[i])(cmdc, cmdv);
                        if (ret_val != 0) goto End;
                        break;
                    }
                }

                if (!flag) {
                    handle_client_error(conn_info->connfd,
                                        "Error: No such command\n");
                }
            }

            cmdc = str_to_vec(NULL, cmdv);
        }
        memset(buf, 0, sizeof(buf));
    }

    if (bytes_read < 0) {
        fprintf(stderr, "Error on reading socket: ");
        handle_error();
    }

End:

    printf("Connection closed\n");
}

int handle_login(int cmdc, char **cmdv) {
    if (cmdc < 3) {
        handle_client_error(this_conn_info->connfd,
                            "Error: LOGIN require username and password\n");
        return 0;
    }

    int flag = 0;
    for (int i = 0; i < userCount; i++) {
        if (strcmp(users[i].username, cmdv[1]) == 0 &&
            strcmp(users[i].passwd, cmdv[2]) == 0) {
            this_conn_info->user = &users[i];

            char str[BUF_SIZE] = {0};
            sprintf(str, "LOGIN_SUCCESS %d %s\n", this_conn_info->user->user_id,
                    this_conn_info->user->username);
            write_client(this_conn_info->connfd, str);

            flag = 1;
            break;
        }
    }

    if (!flag) {
        handle_client_error(this_conn_info->connfd,
                            "Error: wrong username or password\n");
    }

    return 0;
}

int get_online(int cmdc, char **cmdv) {
    sem_wait(&shmem_mutex);

    int online_count = 0;
    conn_info_t *conn;
    LIST_FOREACH(conn, &shmem->conn_infos, entries) {
        if (conn->user != NULL) {
            online_count++;
        }
    }

    user_t *online_users[online_count];
    {
        int idx = 0;

        LIST_FOREACH(conn, &shmem->conn_infos, entries) {
            if (conn->user != NULL) {
                online_users[idx++] = conn->user;
            }
        }
    }
    sem_post(&shmem_mutex);

    char buf[BUF_SIZE] = {0};
    sprintf(&buf[strlen(buf)], "%d\n", online_count);
    for (int i = 0; i < online_count; i++) {
        sprintf(&buf[strlen(buf)], "%d %s\n", online_users[i]->user_id,
                online_users[i]->username);
    }
    write_client(this_conn_info->connfd, buf);

    return 0;
}

int invite_game(int cmdc, char **cmdv) {
    if (cmdc < 2) {
        handle_client_error(this_conn_info->connfd,
                            "Error: INVITE require userid\n");
        return 0;
    }

    if (!user_login_middleware()) return 0;

    int invite_target = atoi(cmdv[1]);

    if (invite_target == this_conn_info->user->user_id) {
        handle_client_error(this_conn_info->connfd,
                            "Error: cannot invite self\n");
        return 0;
    }

    char buf[BUF_SIZE] = {0};
    sprintf(buf, "INV %d %d\n", getpid(), invite_target);
    write_to_pipe(NULL, buf);

    write_client(this_conn_info->connfd, "Invitation sent!\n");

    return 0;
}

int ack_invite(int cmdc, char **cmdv) {
    if (!user_login_middleware()) return 0;

    int ack_user_id = atoi(cmdv[1]);

    char buf[BUF_SIZE] = {0};
    sprintf(buf, "INVACK %d %d\n", getpid(), ack_user_id);
    write_to_pipe(NULL, buf);

    return 0;
}

int rst_invite(int cmdc, char **cmdv) {
    if (!user_login_middleware()) return 0;

    int rst_user_id = atoi(cmdv[1]);

    char buf[BUF_SIZE] = {0};
    sprintf(buf, "INVRST %d %d\n", getpid(), rst_user_id);
    write_to_pipe(NULL, buf);

    return 0;
}

int place_chest(int cmdc, char **cmdv) {
    game_t *game = this_conn_info->current_game;

    if (game->player[game->turn] != this_conn_info) {
        write_client(this_conn_info->connfd, "Error: not your turn\n");
        return 0;
    }

    int x = atoi(cmdv[1]);
    int y = atoi(cmdv[2]);

    if (game->board[x][y] != EMPTY) {
        write_client(this_conn_info->connfd, "Error: space used\n");
        return 0;
    }

    game->board[x][y] = game->turn ? P2 : P1;

    game->turn = !game->turn;

    char buf[BUF_SIZE] = {0};
    sprintf(buf, "GAME_UPDATE %d\n", getpid());
    write_to_pipe(NULL, buf);

    return 0;
}

int handle_logout(int cmdc, char **cmdv) {
    this_conn_info->user = NULL;
    return 0;
}
