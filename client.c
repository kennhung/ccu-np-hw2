#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <ctype.h>

#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <termios.h>
#include <pthread.h>

#include <arpa/inet.h>
#include <sys/socket.h>

#include "game.c"

#define NONE "\033[m"
#define RED "\033[0;32;31m"
#define LIGHT_RED "\033[1;31m"
#define GREEN "\033[0;32;32m"
#define LIGHT_GREEN "\033[1;32m"
#define BLUE "\033[0;32;34m"
#define LIGHT_BLUE "\033[1;34m"
#define DARY_GRAY "\033[1;30m"
#define CYAN "\033[0;36m"
#define LIGHT_CYAN "\033[1;36m"
#define PURPLE "\033[0;35m"
#define LIGHT_PURPLE "\033[1;35m"
#define BROWN "\033[0;33m"
#define YELLOW "\033[1;33m"
#define LIGHT_GRAY "\033[0;37m"
#define WHITE "\033[1;37m"

#define SERV_PORT 10210

#define BUF_SIZE 1024

void handle_error() {
    fprintf(stderr, "Error: %s\n", strerror(errno));
    return;
}

typedef struct {
    int id;
    char name[100];
} player_t;

struct game_board {
    board_pos_status board[3][3];
    int turn;
    player_t player[2];
} game_board;

int sockfd;

// str to vec
char *str_to_vec_ptr = NULL;
int str_to_vec(char *str, char **vector) {
    if (str != NULL) {
        str_to_vec_ptr = str;
    }

    if (str_to_vec_ptr == NULL) return 0;

    char *buf = strtok(str_to_vec_ptr, "\n");
    if (buf == NULL) {
        str_to_vec_ptr = NULL;
        return 0;
    }

    str_to_vec_ptr = buf + strlen(buf) + 1;

    int cmdc = 0;
    {
        char *tok = strtok(buf, " ");
        while (tok != NULL) {
            vector[cmdc] = tok;
            cmdc++;
            tok = strtok(NULL, " ");
        }
    }
    return cmdc;
}

// socket recv
int pipefd[2];
pthread_t socket_recv_pthread;

int user_input_state = 0;  // 0 is menu, 1 is invite_response

int curr_user_id = -1;
int invite_user_id = -1;

void show_game();

void *handle_socket_recv(void *data) {
    char buf[BUF_SIZE] = {0}, buf2[BUF_SIZE] = {0};
    size_t bread;
    while ((bread = read(sockfd, buf, BUF_SIZE - 1)) > 0) {
        memcpy(buf2, buf, sizeof(buf));
        // printf("%s\n", buf2);
        char *msgv[BUF_SIZE];
        int msgc = str_to_vec(buf, msgv);

        if (msgc > 0 && strcmp(msgv[0], "RECV_INVITE") == 0) {
            invite_user_id = atoi(msgv[1]);
            printf("%s wants to invite you to a game.\nAccept? (y/n) ",
                   msgv[2]);
            fflush(stdout);
            user_input_state = 1;
        } else if (msgc > 0 && strcmp(msgv[0], "LOGIN_SUCCESS") == 0) {
            curr_user_id = atoi(msgv[1]);

            printf("Welcome, %s\n", msgv[2]);
        } else if (msgc > 0 && strcmp(msgv[0], "GAME_INFO") == 0) {
            user_input_state = 2;
            int offset = 10;
            int bread;

            // printf("offset %s\n", buf2 + offset);

            sscanf(buf2 + offset, "%d%s%d%s%n", &game_board.player[0].id,
                   game_board.player[0].name, &game_board.player[1].id,
                   game_board.player[1].name, &bread);
            offset += bread;

            sscanf(buf2 + offset, "%d%n", &game_board.turn, &bread);
            offset += bread;

            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    sscanf(buf2 + offset, "%d%n", &game_board.board[i][j],
                           &bread);
                    offset += bread;
                }
            }

            show_game();
        } else {
            if (write(pipefd[1], buf2, strlen(buf2)) < 0) {
                fprintf(stderr, "Error on writing to pipe: ");
                handle_error();
            }
        }

        memset(buf, 0, sizeof(buf));
        memset(buf2, 0, sizeof(buf2));
    }

    pthread_exit(NULL);
}

// Menu
typedef void(menu_handler)();

void handle_login();
void online();
void invite();
void logout();

char *menu_items[] = {"Login", "Online users", "Invite user", "Logout"};
menu_handler *menu_funcs[] = {handle_login, online, invite, logout};
int menu_count = 4;

void show_menu() {
    printf("\nSelect an option\n");
    printf("----------------\n");

    for (int i = 0; i < menu_count; i++) {
        printf(YELLOW "%d) %s\n" NONE, i + 1, menu_items[i]);
    }

    printf("\n");
}

void write_server(int connfd, char *msg);

void user_loop() {
    char buf[BUF_SIZE] = {0};
    fgets(buf, BUF_SIZE, stdin);

    switch (user_input_state) {
        case 0: {
            int selected;
            sscanf(buf, "%d", &selected);
            selected--;
            if (selected < 0 || selected >= menu_count) {
                printf(RED "Wrong option! Select again!\n" NONE);

            } else {
                (menu_funcs[selected])();
            }

            show_menu();

            break;
        }
        case 1: {
            char selected;
            sscanf(buf, " %c", &selected);

            if (selected == 'y') {
                char buf[BUF_SIZE] = {0};
                sprintf(buf, "ACK_INVITE %d\n", invite_user_id);
                write_server(sockfd, buf);
            } else {
                char buf[BUF_SIZE] = {0};
                sprintf(buf, "RST_INVITE %d\n", invite_user_id);
                write_server(sockfd, buf);
            }
            break;
        }
        case 2: {
            int x;
            sscanf(buf, "%d", &x);

            if (x < 0 || x > 8) {
                printf("Wrong pos!!\n");
                break;
            }

            int a = x / 3, b = x % 3;

            if (game_board.board[a][b] != 0) {
                printf("Space used!!\n");
                break;
            }

            char buf[BUF_SIZE] = {0};
            sprintf(buf, "PLACE_CHEST %d %d\n", a, b);
            write_server(sockfd, buf);
        }
        default:
            break;
    }
}

// utils
void write_server(int connfd, char *msg) {
    if (write(connfd, msg, strlen(msg)) < 0) {
        fprintf(stderr, "Error on write_server: ");
        handle_error();
        return;
    }
}

int main(int argc, char **argv) {
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        fprintf(stderr, "Error opening socket: ");
        handle_error();
        exit(1);
    }

    struct sockaddr_in servaddr;

    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(SERV_PORT);

    if (argc < 2) {
        servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
    } else {
        servaddr.sin_addr.s_addr = inet_addr(argv[1]);
    }

    if (connect(sockfd, &servaddr, sizeof(servaddr)) < 0) {
        fprintf(stderr, "Error connect to server: ");
        handle_error();
        exit(1);
    }

    printf("Server connected!\n");

    if (pipe(pipefd) < 0) {
        fprintf(stderr, "Error on creating pipe: ");
        handle_error();
        exit(1);
    }

    if (pthread_create(&socket_recv_pthread, NULL, handle_socket_recv, NULL)) {
        fprintf(stderr, "Error creating pthread: ");
        handle_error();
        exit(1);
    }

    show_menu();

    while (1) {
        user_loop();
    }

    return 0;
}

void run_game();

void show_board() {
    int k = 0;
    printf("+---+---+---+\n");
    for (int i = 0; i < 3; i++) {
        printf("|");
        for (int j = 0; j < 3; j++) {
            printf(" %c |", game_board.board[i][j] == P1   ? 'O'
                            : game_board.board[i][j] == P2 ? 'X'
                                                           : k + '0');
            k++;
        }
        printf("\n+---+---+---+\n");
    }
}

void show_game() {
    printf("Player1 (O): %s\n", game_board.player[0].name);
    printf("Player2 (X): %s\n", game_board.player[1].name);

    int game_end = 0;

    for (int i = 0; i < 3; i++) {
        if (game_board.board[0][i] != 0 &&
            game_board.board[0][i] == game_board.board[1][i] &&
            game_board.board[1][i] == game_board.board[2][i]) {
            printf("Player%d win\n", game_board.board[0][i]);
            game_end = 1;
            break;
        }

        if (game_board.board[i][0] != 0 &&
            game_board.board[i][0] == game_board.board[i][1] &&
            game_board.board[i][1] == game_board.board[i][2]) {
            printf("Player%d win\n", game_board.board[i][0]);
            game_end = 1;
            break;
        }
    }

    if (game_board.board[0][0] != 0 &&
        game_board.board[0][0] == game_board.board[1][1] &&
        game_board.board[1][1] == game_board.board[2][2]) {
        printf("Player%d win\n", game_board.board[0][0]);
        game_end = 1;
    }

    if (game_board.board[2][0] != 0 &&
        game_board.board[2][0] == game_board.board[1][1] &&
        game_board.board[1][1] == game_board.board[0][2]) {
        printf("Player%d win\n", game_board.board[2][0]);
        game_end = 1;
    }

    int have_space = 0;
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            if (game_board.board[i][j] == 0) {
                have_space = 1;
                goto EndHaveSpaceCheck;
            }
        }
    }

EndHaveSpaceCheck:

    if (!have_space) {
        printf("Tie!!\n");
        game_end = 1;
    }

    if (game_end) {
        user_input_state = 0;
        show_board();
        show_menu();
    } else {
        printf("Now is Player%d's turn\n", game_board.turn + 1);
        show_board();
    }
}

void handle_login() {
    char username[100] = {0};
    char *password;

    printf("Enter your username: ");
    scanf("%s", username);

    // printf("Enter your password: ");
    password = getpass("Enter your password: ");

    char buf[BUF_SIZE] = {0};
    sprintf(buf, "LOGIN %s %s\n", username, password);
    write_server(sockfd, buf);

    fgets(buf, BUF_SIZE, stdin);
}

void online() {
    write_server(sockfd, "ONLINE\n");

    char buf[BUF_SIZE] = {0};
    if (read(pipefd[0], buf, BUF_SIZE - 1) < 0) {
        fprintf(stderr, "Error reading socket: ");
        handle_error();
        return;
    }

    int offset = 0;
    int bread;

    // printf("%s\n", buf);

    int online_count;
    sscanf(buf + offset, "%d%n", &online_count, &bread);
    offset += bread;

    printf("Online users:\n\n");
    printf("%-10s%-30s\n", "user_id", "user_name");
    printf("---------------------\n");

    for (int i = 0; i < online_count; i++) {
        int user_id;
        char user_name[1000];

        sscanf(buf + offset, "%d %s %n", &user_id, user_name, &bread);
        offset += bread;

        printf(LIGHT_PURPLE "%-10d" YELLOW "%-30s\n" NONE, user_id, user_name);
    }
}

void invite() {
    printf("Enter user_id of the user you want to invite: ");
    int user_id;
    scanf("%d", &user_id);

    char bbuf[10];
    fgets(bbuf, 10, stdin);

    char buf[BUF_SIZE] = {0};
    sprintf(buf, "INVITE %d\n", user_id);
    write_server(sockfd, buf);

    memset(buf, 0, sizeof(buf));
    if (read(pipefd[0], buf, BUF_SIZE - 1) < 0) {
        fprintf(stderr, "Error reading socket: ");
        handle_error();
    }

    printf("\n%s\n", buf);
}

void logout() { exit(0); }
