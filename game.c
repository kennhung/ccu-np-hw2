#include <string.h>
#include <sys/queue.h>
#include "user.c"

typedef enum { EMPTY, P1, P2 } board_pos_status;

struct conn_info_t;

typedef struct game_t {
    struct conn_info_t *player[2];
    board_pos_status board[3][3];
    int turn;  // 0 for player[0], 1 for player[1]

    LIST_ENTRY(game_t) entries;
} game_t;

LIST_HEAD(game_list_head, game_t);

#define game_pools_sz 100
void *game_pools;
int game_pools_used[game_pools_sz];

game_t *create_game() {
    int idx = -1;
    for (int i = 0; i < game_pools_sz; i++) {
        if (!game_pools_used[i]) {
            idx = i;
            game_pools_used[i] = 1;
            break;
        }
    }

    if (idx == -1) {
        fprintf(stderr, "Error on creating game: no space\n");
        return NULL;
    }

    game_t *game = ((game_t *)game_pools) + idx;

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            game->board[i][j] = EMPTY;
        }
    }

    game->turn = 0;
    game->player[0] = NULL;
    game->player[1] = NULL;

    return game;
}

void free_game(game_t *game) {
    int idx = game - ((game_t *)game_pools);

    memset(game, 0, sizeof(game_t));
    game_pools_used[idx] = 0;
}
